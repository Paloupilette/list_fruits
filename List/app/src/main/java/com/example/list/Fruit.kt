package com.example.list

data class Fruit(var name: String, var img: Int)
val pomme = Fruit("Pomme")
val banane = Fruit("Banane")
val cerise = Fruit("Cerise")
val ananas = Fruit("Ananas")
val abricot = Fruit("Abricot")

