package com.example.list

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fruitArray = arrayOf(Fruit("Pomme"), Fruit("Banane"), Fruit("Cerise"), Fruit("Ananas"), Fruit("Abricot"))
        andVersionRecyclerView.layoutManager = LinearLayoutManager(this)
        andVersionRecyclerView.adapter = FruitAdapter(fruitArray)
    }
    fun seedItems()
    {
        var fruitArray = resources.getStringArray(R.array.Fruit)
        val imgArray = intArrayOf(R.drawable.pomme_background, R.drawable.banane_background, R.drawable.cerise_background, R.drawable.ananas_background, R.drawable.abricot_background)
        for (i in 0..(fruitArray.size-1))
            fruitArray[i] = Fruit(fruitArray[i], imgArray[i])
    }
}